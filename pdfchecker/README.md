# pdfchecker

![Version: 0.1.24](https://img.shields.io/badge/Version-0.1.24-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square)

## Source Code

* <https://gitlab.com/pleio/helm-charts/pdfchecker-charts>

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| amqp.host | string | `nil` |  |
| amqp.password | string | `nil` |  |
| amqp.user | string | `nil` |  |
| cms.image.pullPolicy | string | `"IfNotPresent"` |  |
| cms.image.repository | string | `"registry.gitlab.com/pleio/pdfchecker/cms"` |  |
| cms.image.tag | string | `"latest"` |  |
| cms.replicaCount | string | `nil` |  |
| domain | string | `nil` |  |
| fileStorage.db.db_name | string | `nil` |  |
| fileStorage.db.host | string | `nil` |  |
| fileStorage.db.password | string | `nil` |  |
| fileStorage.db.username | string | `nil` |  |
| fileStorage.existingDataStorage | string | `nil` |  |
| fileStorage.existingSecret | string | `nil` |  |
| fileStorage.host | string | `nil` |  |
| fileStorage.image.pullPolicy | string | `"IfNotPresent"` |  |
| fileStorage.image.repository | string | `"ghcr.io/verapdf/verapdf_file-storage"` |  |
| fileStorage.image.tag | string | `"1.1.1"` |  |
| fileStorage.port | string | `nil` |  |
| fileStorage.replicaCount | string | `nil` |  |
| fileStorage.volume.className | string | `nil` |  |
| fileStorage.volume.storageSize | string | `nil` |  |
| filesCleanup.cron | string | `"*/30 * * * *"` |  |
| filesCleanup.image.pullPolicy | string | `"IfNotPresent"` |  |
| filesCleanup.image.repository | string | `"busybox"` |  |
| filesCleanup.image.tag | string | `"1.37.0"` |  |
| filesCleanup.mmin | string | `"+30"` |  |
| jobService.amqp.concurrency | string | `nil` |  |
| jobService.amqp.listeningQueueMaxSize | string | `nil` |  |
| jobService.amqp.listeningQueueName | string | `nil` |  |
| jobService.amqp.maxConcurrency | string | `nil` |  |
| jobService.amqp.sendingQueueMaxSize | string | `nil` |  |
| jobService.amqp.sendingQueueName | string | `nil` |  |
| jobService.db.db_name | string | `nil` |  |
| jobService.db.host | string | `nil` |  |
| jobService.db.password | string | `nil` |  |
| jobService.db.username | string | `nil` |  |
| jobService.existingSecret | string | `nil` |  |
| jobService.host | string | `nil` |  |
| jobService.image.pullPolicy | string | `"IfNotPresent"` |  |
| jobService.image.repository | string | `"ghcr.io/verapdf/verapdf_job-service"` |  |
| jobService.image.tag | string | `"1.1.1"` |  |
| jobService.port | string | `nil` |  |
| jobService.processingLimit | string | `nil` |  |
| jobService.replicaCount | string | `nil` |  |
| useLetsEncryptCertificate | bool | `false` |  |
| vueConfigJs | string | `nil` |  |
| web.image.pullPolicy | string | `"IfNotPresent"` |  |
| web.image.repository | string | `"registry.gitlab.com/pleio/pdfchecker"` |  |
| web.image.tag | string | `"latest"` |  |
| web.replicaCount | string | `nil` |  |
| wildcardTlsSecretName | string | `""` |  |
| worker.amqp.concurrency | string | `nil` |  |
| worker.amqp.listeningQueueMaxSize | string | `nil` |  |
| worker.amqp.listeningQueueName | string | `nil` |  |
| worker.amqp.maxConcurrency | string | `nil` |  |
| worker.amqp.sendingQueueMaxSize | string | `nil` |  |
| worker.amqp.sendingQueueName | string | `nil` |  |
| worker.existingSecret | string | `nil` |  |
| worker.image.pullPolicy | string | `"IfNotPresent"` |  |
| worker.image.repository | string | `"ghcr.io/verapdf/verapdf_worker"` |  |
| worker.image.tag | string | `"1.1.1"` |  |
| worker.processingLimit | string | `nil` |  |
| worker.processingTimeoutInMin | string | `nil` |  |
| worker.replicaCount | string | `nil` |  |
| worker.resources.limits.memory | string | `nil` |  |
| worker.resources.requests.memory | string | `nil` |  |
