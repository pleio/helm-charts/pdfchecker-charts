{{- define "pdfchecker.name" -}}
{{- .Release.Name | trunc 63 -}}
{{- end -}}

{{- define "pdfchecker.fileStorage.name" -}}
{{- printf "%s-filestorage" (include "pdfchecker.name" . ) -}}
{{- end -}}

{{- define "pdfchecker.jobService.name" -}}
{{- printf "%s-jobservice" (include "pdfchecker.name" . ) -}}
{{- end -}}

{{- define "pdfchecker.worker.name" -}}
{{- printf "%s-worker" (include "pdfchecker.name" . ) -}}
{{- end -}}

{{- define "pdfchecker.web.name" -}}
{{- printf "%s-web" (include "pdfchecker.name" . ) -}}
{{- end -}}

{{- define "pdfchecker.cms.name" -}}
{{- printf "%s-cms" (include "pdfchecker.name" . ) -}}
{{- end -}}

{{- define "pdfchecker.cms.migratejob.name" -}}
{{- printf "%s-cms-migratejob" (include "pdfchecker.name" . ) -}}
{{- end -}}

{{- define "pdfchecker.webConfigMap.name" -}}
{{- printf "%s-web-configmap" (include "pdfchecker.name" . ) -}}
{{- end -}}

{{- define "pdfchecker.filesCleanup.name" -}}
{{- printf "%s-files-cleanup" (include "pdfchecker.name" . ) -}}
{{- end -}}

{{- define "pdfchecker.tlsSecretName" -}}
{{- printf "tls-%s" (include "pdfchecker.name" . ) -}}
{{- end -}}

{{- define "pdfchecker.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "pdfchecker.labels" -}}
helm.sh/chart: {{ include "pdfchecker.chart" .}}
{{ include "pdfchecker.selectorLabels" .}}
{{- end -}}

{{- define "pdfchecker.selectorLabels" -}}
app.kubernetes.io/name: {{include "pdfchecker.name" .}}
app.kubernetes.io/instance: {{.Release.Name}}
{{- if .Component}}
app.kubernetes.io/component: {{.Component}}
{{- end}}
{{- end -}}

{{- define "pdfchecker.storageName" -}}
{{- if .Values.fileStorage.existingDataStorage }}
{{- .Values.fileStorage.existingDataStorage -}}
{{- else }}
{{- printf "%s-filestorage-data" ((include "pdfchecker.name" .)) | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
